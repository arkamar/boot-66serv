#!/usr/bin/bash

RESET="\e[0m"
BOLD="\e[1m"
BLUE="${BOLD}\e[34m"
GREEN="${BOLD}\e[32m"
RED="${BOLD}\e[31m"
YELLOW="${BOLD}\e[33m"

post_install() {
	66-tree -nv3 boot
	66-enable -v3 -t boot boot 
}

post_upgrade() {
	
	backup=$(date +%F-%M)
	printf "${YELLOW}%s${RESET}${BOLD}%s${RESET}\n" ":: make a backup of tree boot: " "boot_backup-${backup}" >&2
	66-tree -C boot_backup-${backup} boot &>/dev/null || 66-tree -nv3 boot
	66-enable -f -t boot boot
}

if 66-info -T boot >&/dev/null; then
	post_upgrade
else 
	post_install
fi
