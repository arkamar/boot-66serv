# boot-66serv

Services and init files to boot a machine using 66 tools with s6 supervision suite

For instructions about [S6 supervision suite.](https://skarnet.org/software/)

[Bug reports](https://forum.obarun.org)

 
